#ifndef CIRCLE_H
#define CIRCLE_H
#include "Ellipse.h"


class Circle : public Ellipse
{
public:
    Circle(float radius);
    Circle();
    virtual ~Circle();
};

#endif // CIRCLE_H
