#ifndef HELIX_H
#define HELIX_H
#include "Circle.h"

#define PI 3.1415f

class Helix : public Circle
{
public:
    Helix(float radius, float step);
    virtual ~Helix();

    virtual Vector3D getPoint(float t);
    virtual Vector3D getFirstDerivative(float t);

private:
    float _step;

};

#endif // HELIX_H
