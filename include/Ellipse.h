#ifndef ELLIPSE_H
#define ELLIPSE_H

struct Vector3D
{
    float x;
    float y;
    float z;
};

class Ellipse
{
public:
    Ellipse(float radiusX, float radiusY);
    Ellipse();
    virtual ~Ellipse();

    virtual Vector3D getPoint(float t);
    virtual Vector3D getFirstDerivative(float t);

    virtual float getRadiusX();
    virtual float getRadiusY();

private:
    float _radiusX;
    float _radiusY;
};

#endif // ELLIPSE_H
