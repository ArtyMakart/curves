#include <cmath>
#include "../include/Ellipse.h"

Ellipse::Ellipse(float radiusX, float radiusY) :
        _radiusX(radiusX), _radiusY(radiusY)
{

}

Ellipse::Ellipse() : _radiusX(0), _radiusY(0)
{

}

Vector3D Ellipse::getPoint(float t)
{
    float x = _radiusX * cos(t);
    float y = _radiusY * sin(t);
    return {x, y, 0};
}

Vector3D Ellipse::getFirstDerivative(float t)
{
    float x = _radiusX * -sin(t);
    float y = _radiusY * cos(t);
    return {x, y, 0};
}

float Ellipse::getRadiusX()
{
    return _radiusX;
}

float Ellipse::getRadiusY()
{
    return _radiusY;
}


Ellipse::~Ellipse()
{

}
