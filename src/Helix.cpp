#include "../include/Helix.h"

Helix::Helix(float radius, float step) : Circle(radius), _step(step)
{
}

Vector3D Helix::getPoint(float t)
{
    Vector3D point = Circle::getPoint(t);
    point.z = _step * t / (2.0f*PI);
    return point;
}

Vector3D Helix::getFirstDerivative(float t)
{
    Vector3D point = Circle::getFirstDerivative(t);
    point.z = _step / (2.0f*PI);
    return point;
}

Helix::~Helix()
{
}
