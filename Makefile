lib:
	g++ -c -fPIC ./include/*.h ./src/*.cpp
	g++ -shared -fPIC -o libcurves.so ./*.o
	rm -f ./*.o ./include/*.gch
	
curves:
	g++ -fopenmp -c ./main.cpp -o main.o
	g++ -o curves main.o -lgomp -L. -lcurves 
	rm -f ./*.o

