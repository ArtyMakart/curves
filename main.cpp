#include <iostream>
#include <vector>
#include <algorithm>
#include "./include/Helix.h"
#include "omp.h"
#define NUM 10
#define MAX_VAL 1000
using namespace std;

float omp_accumulate(const std::vector<Circle*>& circles) {
    float sum = 0;

    #pragma omp parallel for
    for(std::size_t i = 0; i < circles.size(); i++) {
        sum += circles[i]->getRadiusX();
    }

    return sum;
}

int main(int argc, char **argv)
{
    srand(time(0));
    vector<Ellipse*> curves;

    for(int i = 0; i < NUM; i++)
    {
        int random = rand() % 3;
        switch(random)
        {
            case 0:
                curves.push_back(new Ellipse(rand() % MAX_VAL, rand() % MAX_VAL));
            break;
            case 1:
                curves.push_back(new Circle(rand() % MAX_VAL));
            break;
            case 2:
                curves.push_back(new Helix(rand() % MAX_VAL, rand() % MAX_VAL));
            break;
        }
    }

    for(int i = 0; i < NUM; i++)
    {
        Vector3D point = curves[i]->getPoint(PI/4);
        Vector3D firstDerivative = curves[i]->getFirstDerivative(PI/4);
        cout << point.x << " " << point.y << " " << point.z << "  ";
        cout << firstDerivative.x << " "
             << firstDerivative.y << " " << firstDerivative.z << endl;
    }

    vector<Circle*> circles;
    for(int i = 0; i < NUM; i++)
    {
        if(dynamic_cast<Circle*>(curves[i]) != nullptr)
        {
            circles.push_back(dynamic_cast<Circle*>(curves[i]));
        }
    }

    sort(circles.begin(), circles.end(), [](Circle* a, Circle* b) {
                                                                    return a->getRadiusX() <
                                                                        b->getRadiusX();});


    cout << "Sum: " << omp_accumulate(circles) << endl;

    return 0;
}
